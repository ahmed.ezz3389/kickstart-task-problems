<?php
use PHPUnit\Framework\TestCase;
use App\visualization;
header('Content-type:application/json;charset=utf-8');

final class visualizationTest extends TestCase
{
    public function setUp(): void {
        parent::setUp();
        $this->url = "https://spreadsheets.google.com/feeds/list/0Ai2EnLApq68edEVRNU0xdW9QX1BqQXhHRl9sWDNfQXc/od6/public/basic?alt=json";
      }

    public function test_url(): void
     {
        $data = file_get_contents($this->url);
        $result = json_decode($data, true);
        $this->assertArrayHasKey('feed', $result);
    }
    
    public function test_get_url(): void
     {
        $visualization = new visualization();
        $arrays = $visualization->get_url_response($this->url);
        $this->assertArrayHasKey('feed', $arrays);
    }

    public function testResultArray(): void
    {
        $visualization = new visualization();
        $response = $visualization->get_url_response($this->url);
        $json =$visualization->mapJson($response);
        $decoded =json_decode($json, TRUE );           
        foreach($decoded as $array){
                $this->assertArrayHasKey('sentiment', $array);
                $this->assertArrayHasKey('city', $array);
            }
    }
}
