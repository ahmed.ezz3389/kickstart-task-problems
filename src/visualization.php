<?php
namespace App;
header('Content-type:application/json;charset=utf-8');

class visualization
{
    
    public function get_url_response($url)
    {
        $data = file_get_contents("https://spreadsheets.google.com/feeds/list/0Ai2EnLApq68edEVRNU0xdW9QX1BqQXhHRl9sWDNfQXc/od6/public/basic?alt=json");
        $result = json_decode($data, true);
        return $result;
    }
    
    public function mapJson($json)
    {
        $texts = [];
        $regex = '/Cairo|Tanta|Egypt|Brazil|USA|Spain|Ibiza|Damascus|Tahrir|Nairobi|Kathmandu|Bernabau|Madrid|Spain|Athens|Istanbul|Mogadishu/';
        $text = [];
        $data = [];
        $entry =$json['feed']['entry'];
        for($i=0;$i<count($entry);$i++){
            $text[0] = $entry[$i]['content']['$t'];
            preg_match_all($regex, implode(' ', $text) , $matches);
            $data[$i]['city'] = $matches[0];
            $split = explode(" ",  $entry[$i]['content']['$t']);
            $data[$i]['sentiment'] = $split[count($split) - 1];
        }
        $data = json_encode($data);
        echo $data;
    }

}

